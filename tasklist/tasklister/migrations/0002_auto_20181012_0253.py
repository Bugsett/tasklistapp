# Generated by Django 2.1.2 on 2018-10-12 01:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasklister', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='list',
            name='completed_by',
            field=models.CharField(blank=True, default='', max_length=200),
        ),
    ]
