from django.db import models




class List(models.Model):
    item = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    completed = models.BooleanField(default=False)
    creator = models.CharField(max_length=200)
    completed_by = models.CharField(max_length=200, blank=True, default="")

    def __str__(self):
        return self.item + '|' + self.description + '|' + str(self.completed) + '|'+ self.creator + '|' + self.completed_by
