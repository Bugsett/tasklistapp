from django import forms
from django.forms import ModelForm
from .models import List

class ListForm(ModelForm):
    class Meta:
        model = List
        fields = ["item", "description", "completed", "creator", "completed_by"]
