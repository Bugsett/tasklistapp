from django.test import TestCase, RequestFactory
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from .views import home, edit
from .models import List


class test_urls(TestCase):
    def test_home_url(self):
        path = reverse('home', kwargs={})
        assert resolve(path).view_name == 'home'

    def test_hide_url(self):
        path = reverse('hide', kwargs={})
        assert resolve(path).view_name == 'hide'

    def test_unhide_url(self):
        path = reverse('unhide', kwargs={})
        assert resolve(path).view_name == 'unhide'

    def test_signup_url(self):
        path = reverse('signup', kwargs={})
        assert resolve(path).view_name == 'signup'

    def test_deleted_url(self):
        path = reverse('delete', kwargs={'list_id': "1"})
        assert resolve(path).view_name == 'delete'

    def test_edit_url(self):
        path = reverse('edit', kwargs={'list_id': "1"})
        assert resolve(path).view_name == 'edit'

    def test_completed_url(self):
        path = reverse('completed', kwargs={'list_id': "1"})
        assert resolve(path).view_name == 'completed'

    def test_reopen_url(self):
        path = reverse('reopen', kwargs={'list_id': "1"})
        assert resolve(path).view_name == 'reopen'
