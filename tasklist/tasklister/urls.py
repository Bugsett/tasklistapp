from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('delete/<list_id>', views.delete, name="delete"),
    path('completed/<list_id>', views.completed, name="completed"),
    path('reopen/<list_id>', views.reopen, name="reopen"),
    path('edit/<list_id>', views.edit, name="edit"),
    path('signup/', views.signup.as_view(), name='signup'),
    path('hide/', views.hide, name='hide'),
    path('unhide/', views.unhide, name='unhide'),
]
