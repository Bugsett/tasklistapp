from django.test import TestCase
from django.contrib.auth import get_user_model
from .forms import ListForm
from .models import List

class test_forms(TestCase):
    def test_valid_data(self):
        form = ListForm({
            'item': "Test Item",
            'description': "Test Description",
            'completed': False,
            'creator': "Tester",
            'completed_by': "",
        })
        self.assertTrue(form.is_valid())
        List = form.save()
        self.assertEqual(List.item, "Test Item")
        self.assertEqual(List.description, "Test Description")
        self.assertEqual(List.completed, False)
        self.assertEqual(List.creator, "Tester")
        self.assertEqual(List.completed_by, "")

    def test_blank_data(self):
        form = ListForm({})
        self.assertFalse(form.is_valid())
