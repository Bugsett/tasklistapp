from django.urls import resolve
from django.test import TestCase, RequestFactory
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib import messages
from .views import home, edit, delete, completed
from .models import List


class test_views(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='jacob', email='jacob@…', password='top_secret')
        self.list = List.objects.create(
            item="My test item", description="My test description", completed=False, creator="Test User")

    def test_home_page_returns_html(self):
        request = self.factory.get('/')
        request.user = self.user
        response = home(request)
        self.assertEqual(response.status_code, 200)
        html = response.content.decode('utf8')
        self.assertIn('<title>Task List Manager</title>', html)

    def test_edit_page_returns_html(self):
        request = self.factory.get('/edit/'+str(self.list.id))
        request.user = self.user
        response = edit(request, str(self.list.id))
        self.assertEqual(response.status_code, 200)
        html = response.content.decode('utf8')
        self.assertIn('<title>Edit Task</title>', html)
