from django.shortcuts import render, redirect
from .models import List
from .forms import ListForm
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.models import User


def home(request):
    if request.method == 'POST':
        form = ListForm(request.POST or None)

        if form.is_valid():
            form.save()
            all_items = List.objects.all
            completed_items = List.objects.filter(completed=True)
            own_items = List.objects.filter(creator=request.user.get_username())
            hidden = False
            messages.success(request, ('Task has been added to list.'))
            return render(request, 'home.html', {'all_items': all_items, 'own_items': own_items, 'completed_items': completed_items, 'hidden': hidden})
        else:
            all_items = List.objects.all
            completed_items = List.objects.filter(completed=True)
            own_items = List.objects.filter(creator=request.user.get_username())
            hidden = False
            messages.success(request, (form.errors))
            return render(request, 'home.html', {'all_items': all_items, 'own_items': own_items, 'completed_items': completed_items, 'hidden': hidden})

    else:
        all_items = List.objects.all
        completed_items = List.objects.filter(completed=True)
        own_items = List.objects.filter(creator=request.user.get_username())
        hidden = False
        return render(request, 'home.html', {'all_items': all_items, 'own_items': own_items, 'completed_items': completed_items, 'hidden': hidden})

def delete(request, list_id):
    item = List.objects.get(pk=list_id)
    item.delete()
    messages.success(request, ('Task has been deleted.'))
    return redirect('home')

def completed(request, list_id):
    item = List.objects.get(pk=list_id)
    item.completed = True
    item.completed_by = request.user.get_username()
    item.save()
    return redirect('home')

def reopen(request, list_id):
    item = List.objects.get(pk=list_id)
    item.completed = False
    item.completed_by = ""
    item.save()
    return redirect('home')

def edit(request, list_id):
    if request.method == 'POST':
        item = List.objects.get(pk=list_id)
        form = ListForm(request.POST or None, instance=item)
        if form.is_valid():
            form.save()
            messages.success(request, ('Task has been changed.'))
            return redirect('home')
    else:
        item = List.objects.get(pk=list_id)
        return render(request, 'edit.html', {'item': item})

def hide(request):
    all_items = List.objects.all
    completed_items = List.objects.filter(completed=True)
    own_items = List.objects.filter(creator=request.user.get_username())
    hidden = True
    return render(request, 'home.html', {'all_items': all_items, 'own_items': own_items, 'completed_items': completed_items, 'hidden': hidden})

def unhide(request):
    all_items = List.objects.all
    completed_items = List.objects.filter(completed=True)
    own_items = List.objects.filter(creator=request.user.get_username())
    hidden = False
    return render(request, 'home.html', {'all_items': all_items, 'own_items': own_items, 'completed_items': completed_items, 'hidden': hidden})

class signup(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'
