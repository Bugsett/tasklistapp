from django.apps import AppConfig


class TasklisterConfig(AppConfig):
    name = 'tasklister'
