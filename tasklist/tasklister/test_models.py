from django.test import TestCase
from .models import List


class test_models(TestCase):

    def create_model_test(self, item="My test item", description="My test description", completed=False, creator="Test User"):
        return List.objects.create(item=item, description=description, completed=completed, creator=creator)

    def test_model_creation(self):
        test = self.create_model_test()
        self.assertTrue(isinstance(test, List))
        self.assertEqual(test.__str__(), test.item + '|' + test.description + '|' + str(test.completed) + '|' + test.creator + '|')
