Hello!

To run this program we're going to assume you have both Python, Django, and Git installed.

clone this repository to where you want it and from there open cmd.

Once cmd has been opened run the command "python manage.py runserver". (from \Django\tasklistapp\tasklist folder)

Once the server is running you can open up the program in your browser of choice. 

Open http://127.0.0.1:8000/#

From there you need to log in. There are two users already created, see bottom of this text file to find them, or you could create your own.

Once logged in, everything should hopefully be quite self-explanatory. You can edit and delete tasks you create and you can take it upon yourself to finish others. 

Add as many tasks as you feel like and hide the completed ones if you want.

Happy Tasking!

Users/password:
admin/Passord321
Dave/testMcTest

----

Testing:
To run the tests, be in \Django\tasklistapp\tasklist folder and run "python manage.py test tasklister".

Time spent:
Probably around 10 hours over a few differnt days. I haven't had the most time available for this challange, unfortunately, you've cought me at a bit of a bad time! So the result was late night work which also added to the time spent, and I'd say lowered the level of the code a bit too. 