
from django.contrib import admin
from django.urls import path, include
from tasklister import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('django.contrib.auth.urls')),
    path('', include('tasklister.urls')),
]
